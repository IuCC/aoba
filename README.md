# Aoba Hacked Client (1.20.2)
A custom hacked client for Minecraft 1.20.2 using Fabric. 

(Fork of https://github.com/coltonk9043/Aoba-MC-Hacked-Client. I do NOT own this client, I just forked it to add more features and customize it to my likings)

## Features
- Fully featured Alt Manager with MCLeaks integration.
- Working Command System.
- ClickGui with moveable/pinnable windows and customization.
- Plenty of hacks.

## Default Keybinds:
RSHIFT - Toggle ClickGUI.\
↑ - Move menu 'cursor' up.\
↓ - Move menu 'cursor' down.\
← - Leave current 'mod' menu.\
→ - Enter currently selected 'mod' menu.\
K - Toggle Aimbot.\
N - Toggle AutoEat.\
V - Toggle Fly.\
Z - Toggle NoSlowdown.\
G - Toggle Sprint.\
F - Toggle Fullbright.\
X - Toggle XRay.\

## Screenshots
![image](https://cdn.discordapp.com/attachments/1190306124314976387/1205174813627256893/FF56COq.png?ex=65d76982&is=65c4f482&hm=a98ab6edbb4a9ed0abdc0666665c7f27bf7f5d57abe58f24a71b2f998949b9f2&)
![image](https://cdn.discordapp.com/attachments/1190306124314976387/1205174814344613898/UNaEh5X.png?ex=65d76982&is=65c4f482&hm=9fa711b12dc4f37023488fea94de9098cabc9cc8d905a72fe69eca3a91802142&)
![image](https://cdn.discordapp.com/attachments/1190306124314976387/1205174815254781952/Al4GzO8.png?ex=65d76983&is=65c4f483&hm=f6fb5ae325eb9c0d974101201098aea91fa59fee900f5ca5a6061413c4ac3aa0&)
![image](https://cdn.discordapp.com/attachments/1190306124314976387/1205174816072540160/mE1VHtb.png?ex=65d76983&is=65c4f483&hm=09ae118ef1910445f6bb7ede552bab040eecb04604cc8de855668069392719ca&)

## License
This code is licensed under the GNU General Public License v3. You may use code published here in your own clients under the same license.