package net.aoba.misc;

import net.aoba.gui.Color;

public class Colors {
	public static final Color Red = new Color(255, 0, 0);
	public static final Color Orange = new Color(255, 165, 0);
	public static final Color Yellow = new Color(255, 255, 0);
	public static final Color Green = new Color(0, 255, 0);
	public static final Color Cyan = new Color(0, 255, 255);
	public static final Color Blue = new Color(0, 0, 255);
	public static final Color Purple = new Color(255, 0, 255);
	public static final Color Pink = new Color(255, 192, 203);
	public static final Color Black = new Color(0, 0, 0);
	public static final Color Charcoal = new Color(54, 69, 79);
	public static final Color Gray = new Color(127, 127, 127);
	public static final Color White = new Color(255, 255, 255);
}
