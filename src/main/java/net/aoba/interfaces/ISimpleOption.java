package net.aoba.interfaces;

public interface ISimpleOption<T>
{
	public void forceSetValue(T newValue);
}
