/*
* Aoba Hacked Client
* Copyright (C) 2019-2023 coltonk9043
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Timer Module
 */
package net.aoba.module.modules.misc;

import net.aoba.Aoba;
import net.aoba.module.Module;
import net.aoba.settings.types.KeybindSetting;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.util.InputUtil;
import net.minecraft.text.Text;
import net.minecraft.text.TextContent;
import org.lwjgl.glfw.GLFW;

public class VulcanCheck extends Module {

	public VulcanCheck() {
		super(new KeybindSetting("key.vulcancheck", "Vulcan Check", InputUtil.fromKeyCode(GLFW.GLFW_KEY_UNKNOWN, 0)));
		
		this.setName("Vulcan Check");
		this.setCategory(Category.Misc);
		this.setDescription("Checks if the server is vulnerable for the Vulcan force-op exploit.");
	}
	
	@Override
	public void onDisable() {
	}

	@Override
	public void onEnable() {
		this.toggle();
		ClientPlayerEntity player = Aoba.getInstance().MC.player;

		Thread commandThread = new Thread(() -> {
			player.sendMessage(Text.literal("§7Trying §d/ver Vulcan"));
			sendCommandWithDelay(player, "ver Vulcan", 1000);
			player.sendMessage(Text.literal("§7Trying §d/about Vulcan"));
			sendCommandWithDelay(player, "about Vulcan", 1000);
			player.sendMessage(Text.literal("§7Trying §d/bukkit:ver Vulcan"));
			sendCommandWithDelay(player, "bukkit:ver Vulcan", 1000);
			player.sendMessage(Text.literal("§7Trying §d/bukkit:about Vulcan"));
			sendCommandWithDelay(player, "bukkit:about Vulcan", 1000);
			player.sendMessage(Text.literal("§7Trying §d/vulcan"));
			sendCommandWithDelay(player, "vulcan", 1000);
			player.sendMessage(Text.literal("§7If you see a message about the Vulcan version being below 2.8.5, the server §oshould§r§7 be vulnerable to the Vulcan force op exploit."));
		});
		commandThread.start();
	}

	private void sendCommandWithDelay(ClientPlayerEntity player, String command, int delay) {
		player.networkHandler.sendChatCommand(command);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onToggle() {
	}
}